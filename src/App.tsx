import React from 'react';
// import 'antd/dist/antd.css';
import './App.less';
import Container from './components/Container/container';
import Dashboard from './views/Dashboard/dashboard';
import Lebensmittel from './views/Lebensmittel/lebensmittel';

function App() {
    return (
        <Container>
            <div style={{ display: 'flex', justifyContent: 'space-evenly', height: '100%', flexWrap: 'wrap' }}>
                <Lebensmittel />
                <Lebensmittel />
                <Lebensmittel />
                <Lebensmittel />
                <Lebensmittel />
            </div>
        </Container>
    );
}

export default App;
