import React from 'react';
import { Card, Avatar } from 'antd';
import { HeartFilled } from '@ant-design/icons';

const Lebensmittel = () => {
    const { Meta } = Card;
    return (
        <div>
            <Card
                style={{ width: 300 }}
                cover={
                    <img
                        alt="example"
                        src="https://shop.vegavero.com/mediafiles/Bilder/combinazione-alimenti-favorire-la-digestione.jpg"
                    />
                }
                actions={[<HeartFilled key="ellipsis" />]}
            >
                <Meta
                    // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                    title="Lebensmittel"
                    description="This is the description"
                />
            </Card>
        </div>
    );
};

export default Lebensmittel;
